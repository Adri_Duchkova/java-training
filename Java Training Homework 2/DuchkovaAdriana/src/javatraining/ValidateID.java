package javatraining;
/**
 * Interface class has is CzechID and isFinnishID methods
 * @author adri
 *
 */
public interface ValidateID {
	/**
	 * checks if source string corresponds to Czech ID format 
	 * Czech ID format YYMMDD/SSSC ,10-11 characters, where MM (numbers 01-12)or(numbers 51-62), SSS serial number, C check digit(except years below 1954),whole number must be divisible by 11(except years below 1954)
	 * @param source string
	 * @return true if corresponds to Finnish ID
	 */
	boolean isCzechID(String source);
	/**
	 * checks if source string corresponds to Finnish ID format 
	 * Finnish Id format DDMMYYCZZZQ, where DDMMYY is the DOB, C (+-A), ZZZ individual number from 002 to 899, Q the control character (checksum, reminder from division by 31)
	 * @param source string
	 * @return true if corresponds to Finnish ID
	 */
	boolean isFinnishID(String source);
	
	
}
