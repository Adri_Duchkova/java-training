package javatraining;

import org.junit.Test;

import junit.framework.TestCase;

public class TestIsCzechID extends TestCase{

	ValidateIDImpl testedId = new ValidateIDImpl();
	
	@Test
	public void testIsCzechIDNull() {
		boolean result = testedId.isCzechID(null);
		assertFalse(result);
	}
	@Test
	public void testIsCzechIDEmptyString() {
		boolean result = testedId.isCzechID("");
		assertFalse(result);
	}
	@Test
	public void testIsCzechIDShortString() {
		boolean result = testedId.isCzechID("811120");
		assertFalse(result);
	}
	@Test
	public void testIsCzechIDNineCharactersString() {
		boolean result = testedId.isCzechID("811120/22");
		assertFalse(result);
	}
	@Test
	public void testIsCzechIDTwelveCharactersString() {
		boolean result = testedId.isCzechID("816120/20386");
		assertFalse(result);
	}
	@Test
	public void testIsCzechIDLongString() {
		boolean result = testedId.isCzechID("8161200/20386");
		assertFalse(result);
	}
	@Test
	public void testIsCzechIDTenCharactersYearsBF54() {
		boolean result = testedId.isCzechID("530615/929");
		assertTrue(result);
	}
	
	@Test
	public void testIsCzechIDTenCharactersYearsAfter54() {
		boolean result = testedId.isCzechID("850615/929");
		assertFalse(result);
	}
	@Test
	public void testIsCzechIDElevenCharactersYearBF54() {
		boolean result = testedId.isCzechID("536120/2038");
		assertFalse(result);
	}
	@Test
	public void testIsCzechIDElevenCharactersYear54() {
		boolean result = testedId.isCzechID("546116/1904");
		assertTrue(result);
	}
	@Test
	public void testIsCzechIDElevenCharactersAfter54() {
		boolean result = testedId.isCzechID("816120/2038");
		assertTrue(result);
	}
	
	@Test
	public void testIsCzechIDWithLetters() {
		boolean result = testedId.isCzechID("8A6120/2038");
		assertFalse(result);
	}
	@Test
	public void testIsCzechIDWithDifferentChar() {
		boolean result = testedId.isCzechID("8.6120/2038");
		assertFalse(result);
	}
	@Test
	public void testIsCzechIDWithDifferentCharInsteadSlash() {
		boolean result = testedId.isCzechID("816120.2038");
		assertFalse(result);
	}
	@Test
	public void testIsCzechIDWithoutSlash() {
		boolean result = testedId.isCzechID("81612002038");
		assertFalse(result);
	}
	@Test
	public void testIsCzechIDWithSlashNotSeventhPosition() {
		boolean result = testedId.isCzechID("8161202/038");
		assertFalse(result);
	}
	@Test
	public void testIsCzechIDDivisibleByEleven() {
		boolean result = testedId.isCzechID("816120/2038");
		assertTrue(result);
	}
	@Test
	public void testIsCzechIDNonDivisibleByEleven() {
		boolean result = testedId.isCzechID("816120/2039");
		assertFalse(result);
	}

	@Test
	public void testIsCzechID() {
		boolean result = testedId.isCzechID("816120/2038");
		assertTrue(result);
	}
	@Test
	public void testdivisibleByEleven() {
		boolean result = testedId.divisibleByEleven("11");
		assertTrue(result);
	}
	@Test
	public void testRegexValidationCzechId() {
		boolean result = testedId.regexValidationCzechId("816120/2038");
		assertTrue(result);
	}
	@Test
	public void testRegexValidationCzechIdWrongDate() {
		boolean result = testedId.regexValidationCzechId("811131/2038");
		assertFalse(result);
	}

}
