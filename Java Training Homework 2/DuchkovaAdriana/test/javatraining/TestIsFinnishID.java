package javatraining;

import org.junit.Test;

import junit.framework.TestCase;

public class TestIsFinnishID extends TestCase{

	ValidateIDImpl testedId = new ValidateIDImpl();
	
	@Test
	public void testIsFinnishID() {
		boolean result = testedId.isFinnishID("131052-308T");
		assertTrue(result);
	}
	@Test
	public void testIsFinnishIDNull() {
		boolean result = testedId.isFinnishID(null);
		assertFalse(result);
	}
	@Test
	public void testIsFinnishIDEmptyString() {
		boolean result = testedId.isFinnishID("");
		assertFalse(result);
	}
	@Test
	public void testIsFinnishIDShortString() {
		boolean result = testedId.isFinnishID("201181");
		assertFalse(result);
	}
	@Test
	public void testIsFinnishIDLongString() {
		boolean result = testedId.isFinnishID("201181+000A0");
		assertFalse(result);
	}
	@Test
	public void testIsFinnishIDNotIdFormat() {
		boolean result = testedId.isFinnishID("hjkr6*11111");
		assertFalse(result);
	}
	@Test
	public void testIsFinnishPlusSign() {
		boolean result = testedId.isFinnishID("311280+888Y");
		assertTrue(result);
	}
	@Test
	public void testIsFinnishACharacter() {
		boolean result = testedId.isFinnishID("311280A888Y");
		assertTrue(result);
	}
	@Test
	public void testIsFinnishMinusCharacter() {
		boolean result = testedId.isFinnishID("311280-888Y");
		assertTrue(result);
	}
	@Test
	public void testIsFinnishLastCharacterNotEqualsReminderOf31Division() {
		boolean result = testedId.isFinnishID("311280-888X");
		assertFalse(result);
	}
	@Test
	public void testIsFinnishZZZIndividualNumbers000() {
		boolean result = testedId.isFinnishID("311280-000A");
		assertFalse(result);
	}
	@Test
	public void testIsFinnishZZZIndividualNumbers001() {
		boolean result = testedId.isFinnishID("311280-001B");
		assertFalse(result);
	}
	@Test
	public void testIsFinnishZZZIndividualNumbersHigher899() {
		boolean result = testedId.isFinnishID("311280-900B");
		assertFalse(result);
	}
	
	@Test
	public void testRegexValidationFinnishId() {
		boolean result = testedId.regexValidationFinnishId("301280+887T");
		assertTrue(result);
	}
	@Test
	public void testRegexValidationFinnishIdWrongDate() {
		boolean result = testedId.regexValidationFinnishId("311180+887T");
		assertFalse(result);
	}
	@Test
	public void testLastDigitEgualsToReminderFromDivision() {
		boolean result = testedId.lastDigitEgualsToReminderFromDivision("41A");
		assertTrue(result);
	}
}
