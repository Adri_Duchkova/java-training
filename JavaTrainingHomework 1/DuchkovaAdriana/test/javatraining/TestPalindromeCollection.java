package javatraining;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

public class TestPalindromeCollection {

	PalindromeImpl p = new PalindromeImpl();
	
	@Test
	public void testIfNullCollectionReturnsEmptyCollection()
	{
		Collection<String> pals = p.palindromeCollection(null);
		assertEquals(0, pals.size());
	}
	@Test
	public void testPalindromeCollection() {
		Collection<String> pals = new ArrayList <String>();
		pals.add("ahoj");pals.add("kajak");pals.add("nekajak");pals.add("madam");
		pals.add("krk");
		Collection<String> result = p.palindromeCollection(pals);
		Collection<String> sortList = new ArrayList<String>();
		sortList.add("kajak");sortList.add("madam");sortList.add("krk");
		assertEquals(sortList, result);		
	}
	@Test
	public void testEmptyCollectionPalindromeColletion() {
		Collection<String> pals = new ArrayList <String>();
		Collection<String> result = p.palindromeCollection(pals);
		Collection<String> sortList = new ArrayList<String>();
		assertEquals(sortList, result);		
	}

}
