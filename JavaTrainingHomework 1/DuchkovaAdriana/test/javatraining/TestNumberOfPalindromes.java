package javatraining;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

public class TestNumberOfPalindromes {

	PalindromeImpl p = new PalindromeImpl();
	
	@Test
	public void testCountTrivialPalindrome() {
		Collection<String> pals = new ArrayList <String>();
		pals.add("ahoj");pals.add("kajak");pals.add("nekajak");pals.add("madam");
		pals.add("krk");
		int result = p.numberOfPalindromes(pals);
		assertEquals(3, result);		
	}
	@Test
	public void testCountPalindrome() {
		Collection<String> pals = new ArrayList <String>();
		pals.add("");pals.add(" ");pals.add("...");pals.add("mAdam");
		pals.add("Zeman nem� k�men na mez");
		int result = p.numberOfPalindromes(pals);
		assertEquals(2, result);		
	}

}
