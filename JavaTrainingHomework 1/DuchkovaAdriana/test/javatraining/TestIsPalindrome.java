package javatraining;


import junit.framework.*;
import org.junit.Test;

import javatraining.PalindromeImpl;

import java.util.*;

public class TestIsPalindrome extends TestCase{
	
	PalindromeImpl p = new PalindromeImpl();
	Collection<String> pals = new ArrayList <String>();
	
	@Test
	public void testNullIsNotPalindrome() {
		boolean result = p.isPalindrome(null);
		assertFalse(result);
	}
	@Test
	public void testEmptyStringisNotPalindrome(){
		boolean result = p.isPalindrome("");
		assertFalse(result);
	}
	@Test
	public void testShortStringisNotPalindrome(){
		boolean result = p.isPalindrome("j");
		assertFalse(result);
	}
	@Test
	public void testTrivialPalindrome(){
		boolean result = p.isPalindrome("kajak");
		assertTrue(result);
	}
	@Test
	public void testContainingSpacesPalindrome(){
		boolean result = p.isPalindrome("ka    jak");
		assertTrue(result);
	}
	@Test
	public void testDifferentCasePalindrome(){
		boolean result = p.isPalindrome("KAjak");
		assertTrue(result);
	}
	@Test
	public void testDiactriticsPalindrome(){
		boolean result = p.isPalindrome("Zeman nem� k�men na mez");
		assertTrue(result);
	}
	@Test
	public void testNonLettersPalindrome(){
		boolean result = p.isPalindrome("1..!4!..1");
		assertTrue(result);
	}
	@Test
	public void testCountTrivialPalindrome() {
		pals.add("ahoj");pals.add("kajak");pals.add("nekajak");pals.add("madam");
		pals.add("krk");
		int result = p.numberOfPalindromes(pals);
		assertEquals(3, result);		
	}
	@Test
	public void testCountPalindrome() {
		pals.add("");pals.add(" ");pals.add("...");pals.add("mAdam");
		pals.add("Zeman nem� k�men na mez");
		int result = p.numberOfPalindromes(pals);
		assertEquals(3, result);		
	}
	@Test
	public void testPalindromeList() {
		pals.add("ahoj");pals.add("kajak");pals.add("nekajak");pals.add("madam");
		pals.add("krk");
		Collection<String> result = p.palindromeCollection(pals);
		Collection<String> sortList = new ArrayList<String>();
		sortList.add("kajak");sortList.add("madam");sortList.add("krk");
		assertEquals(sortList, result);		
	}
	@Test
	public void testEmptyListPalindromeList() {
		Collection<String> result = p.palindromeCollection(pals);
		Collection<String> sortList = new ArrayList<String>();
		assertEquals(sortList, result);		
	}

}
