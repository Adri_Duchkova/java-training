package javatraining;

import java.util.*;
import java.text.Normalizer;

public class PalindromeImpl implements Palindrome{

	@Override
	public boolean isPalindrome(String source) {
		if (source ==null){
			return false;
		}
		else {
			source = specialCharactersRemovalLowerCase(source);		
			if(source.length()>2){
				int start = 0;
				int end = source.length()-1;
				int half = end/2;
				
				for (int i = 0; i < half; i++, start++, end--) {
					if(source.charAt(start)!=source.charAt(end))
					return false;	
					}
				return true;
			}
			return false;
		}
	}
	
	/**
	 * deletes all empty spaces and accents, convert to lower case
	 * @return new source string
	 */
	private String specialCharactersRemovalLowerCase(String source){
		 source = source.replaceAll("\\s+","");
		 source = Normalizer.normalize(source, Normalizer.Form.NFD);
		 source = source.replaceAll("[^\\p{ASCII}]", "");
		 source = source.toLowerCase();
		 return source;
		
	}

	@Override
	public int numberOfPalindromes(Collection <String> a) {
		int pals =0;
		for (String arrWords: a) {
			if(isPalindrome(arrWords)){pals++;}
		} 
		return pals;
	}

	@Override
	public Collection<String> palindromeCollection(Collection<String> c) {
		ArrayList<String> palindr = new ArrayList<String>();
		for (String arrWords: c) {
			if(isPalindrome(arrWords)){palindr.add(arrWords);}
		} 
		return palindr;
	}

}
