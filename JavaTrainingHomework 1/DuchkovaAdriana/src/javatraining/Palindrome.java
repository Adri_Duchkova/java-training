package javatraining;
import java.util.*;

/**
 * Interface class that has isPalindrome, numberOfPalindrome and palindromeList methods.
 */
public interface Palindrome {
/**
 * This method examines if given text is palindrome
 * @param s text examined for palindrome
  */
boolean isPalindrome(String s);
/**
 * This method returns number of words which are palindome
 * @param a array of words
 * @return number of palindrome words
 */
int numberOfPalindrome(ArrayList<String> a);
/**
 * This method returns the array of words which are palindrome
 * @param c array of words
 * @return subarray of palindrome words
 */
ArrayList<String> palindromeList(ArrayList<String> c);

}



